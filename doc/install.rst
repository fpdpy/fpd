.. _install:

**********
Installing
**********

The package is is actively developed and new versions are released to the python
package `repository <https://pypi.python.org/pypi/fpd>`_. The source is hosted on 
`gitlab <https://gitlab.com/fpdpy/fpd>`_ for both the development (master branch)
and released (release branch) versions.

Requirements
============
Python version 3.x is currently supported (Python 2.x support was dropped in v0.1.12).
The fpd package depends upon the following packages:

* numpy
* scipy
* scikit-image
* matplotlib
* h5py>=2.10.0
* tqdm
* threadpoolctl
* hdf5plugin
* psutil

With the following optional packages:

* hyperspy

Most of the required packages are included as standard in the majority of distributions, and all
can be installed through `pip`, either manually or automatically when installing the fpd
package.

HyperSpy has many useful features and is an optional dependency in the fpd package (as of v0.2.5)
used for reading Digital Micrograph files. Almost all of the fpd package can be used without
Hyperspy being installed.


Installation
============

Release version
---------------
The easiest way to install the package on any platform is to use pip (or pip3):

.. code-block:: bash

    pip3 install --user fpd

The ``--user`` option installs the package to your local folder, avoiding any file
permission issues.

Development Version
-------------------
To install from source, download or clone the master (development) version
from the repository and, in the package directory, run:

.. code-block:: bash

    pip3 install --user .

To clone the repository, open a terminal and run:

.. code-block:: bash

    git clone git@gitlab.com:fpdpy/fpd.git

An archive can be downloaded from `here <https://gitlab.com/fpdpy/fpd/repository/master/archive.zip>`_.

Upgrading and Uninstalling
--------------------------
In all of the above install commands, ``-U`` can be added to force an upgrade 
or reinstall. When used in combination with ``--no-deps``, only the fpd 
package will be reinstalled.

The package can be removed with:

.. code-block:: bash

    pip3 uninstall fpd


Optional Dependences
--------------------
The above commands install only the required dependences. To also install HyperSpy
at the same time, simply add ``[HS]`` to the package name:

.. code-block:: bash

    pip3 uninstall fpd[HS]
    
HypserSpy has many dependences and can sometimes be difficult to install. Full install
instructions for it can be found at: http://hyperspy.org/hyperspy-doc/current/user_guide/install.html
